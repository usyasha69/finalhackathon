package com.usyasha69.pk.finalhackathon.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class WeatherModel {
    public Data data;

    @Override
    public String toString() {
        return "WeatherModel{" +
                "data=" + data +
                '}';
    }

    public class Data {
        public ArrayList<Request> request;
        @SerializedName("current_condition")
        public ArrayList<CurrentCondition> currentCondition;
        public ArrayList<Weather> weather;

        @Override
        public String toString() {
            return "Data{" +
                    "request=" + request.toString() +
                    ", currentCondition=" + currentCondition.toString() +
                    ", weather=" + weather.toString() +
                    '}';
        }

        public class Request {
            public String type;
            public String query;

            @Override
            public String toString() {
                return "Request{" +
                        "type='" + type + '\'' +
                        ", query='" + query + '\'' +
                        '}';
            }
        }

        public class CurrentCondition {
            @SerializedName("temp_C")
            public String tempC;
            @SerializedName("temp_F")
            public String tempF;
            public ArrayList<WeatherIconUrl> weatherIconUrl;
            public ArrayList<WeatherIconDesc> weatherDesc;
            @SerializedName("windspeedKmph")
            public String windSpeedKmph;
            public String humidity;
            public String visibility;
            public String pressure;
            @SerializedName("FeelsLikeC")
            public String feelsLikeC;
            @SerializedName("FeelsLikeF")
            public String feelsLikeF;

            @Override
            public String toString() {
                return "CurrentCondition{" +
                        "tempC='" + tempC + '\'' +
                        ", tempF='" + tempF + '\'' +
                        ", weatherIconUrl=" + weatherIconUrl.toString() +
                        ", weatherIconDescs=" +weatherDesc.toString() +
                        ", windSpeedKmph='" + windSpeedKmph + '\'' +
                        ", humidity='" + humidity + '\'' +
                        ", visibility='" + visibility + '\'' +
                        ", pressure='" + pressure + '\'' +
                        ", feelsLikeC='" + feelsLikeC + '\'' +
                        ", feelsLikeF='" + feelsLikeF + '\'' +
                        '}';
            }

            public class WeatherIconUrl {
                String value;

                @Override
                public String toString() {
                    return "WeatherIconUrl{" +
                            "value='" + value + '\'' +
                            '}';
                }
            }

            public class WeatherIconDesc {
                String value;

                @Override
                public String toString() {
                    return "WeatherIconDesc{" +
                            "value='" + value + '\'' +
                            '}';
                }
            }
        }

        public class Weather {
            public String date;
            public ArrayList<Astronomy> astronomy;
            public ArrayList<Hourly> hourly;

            @Override
            public String toString() {
                return "Weather{" +
                        "date='" + date + '\'' +
                        ", astronomies=" + astronomy.toString() +
                        ", hourlies=" + hourly.toString() +
                        '}';
            }

            public class Astronomy {
                public String sunrise;
                public String sunset;

                @Override
                public String toString() {
                    return "Astronomy{" +
                            "sunrise='" + sunrise + '\'' +
                            ", sunset='" + sunset + '\'' +
                            '}';
                }
            }

            public class Hourly {
                public String tempC;
                public String tempF;
                public ArrayList<WeatherIconUrl> weatherIconUrl;
                public ArrayList<WeatherIconDesc> weatherDesc;
                @SerializedName("windspeedKmph")
                public String windSpeedKmph;
                public String humidity;
                public String visibility;
                public String pressure;
                @SerializedName("FeelsLikeC")
                public String feelsLikeC;
                @SerializedName("FeelsLikeF")
                public String feelsLikeF;
                @SerializedName("cloudcover")
                public String cloudCover;

                @Override
                public String toString() {
                    return "Hourly{" +
                            "tempC='" + tempC + '\'' +
                            ", tempF='" + tempF + '\'' +
                            ", weatherIconUrl=" + weatherIconUrl.toString() +
                            ", weatherIconDescs=" + weatherDesc.toString() +
                            ", windSpeedKmph='" + windSpeedKmph + '\'' +
                            ", humidity='" + humidity + '\'' +
                            ", visibility='" + visibility + '\'' +
                            ", pressure='" + pressure + '\'' +
                            ", feelsLikeC='" + feelsLikeC + '\'' +
                            ", feelsLikeF='" + feelsLikeF + '\'' +
                            ", cloudCover='" + cloudCover + '\'' +
                            '}';
                }

                public class WeatherIconUrl {
                    public String value;

                    @Override
                    public String toString() {
                        return "WeatherIconUrl{" +
                                "value='" + value + '\'' +
                                '}';
                    }
                }

                public class WeatherIconDesc {
                    public String value;

                    @Override
                    public String toString() {
                        return "WeatherIconDesc{" +
                                "value='" + value + '\'' +
                                '}';
                    }
                }
            }
        }
    }
}
