package com.usyasha69.pk.finalhackathon.UI;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.usyasha69.pk.finalhackathon.R;
import com.usyasha69.pk.finalhackathon.adapters.ExampleRVAdapter;
import com.usyasha69.pk.finalhackathon.models.WeatherModel;
import com.usyasha69.pk.finalhackathon.net.QueryManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    protected RecyclerView.LayoutManager mLayoutManager;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        QueryManager.getWeather(new Callback<WeatherModel>() {
            @Override
            public void success(WeatherModel weatherModel, Response response) {
                System.out.println(weatherModel.toString());

                mRecyclerView.setAdapter(new ExampleRVAdapter(weatherModel.data.weather, MainActivity.this));
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });


    }
}
