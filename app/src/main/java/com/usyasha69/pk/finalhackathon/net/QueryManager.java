package com.usyasha69.pk.finalhackathon.net;

import com.usyasha69.pk.finalhackathon.models.WeatherModel;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;

public class QueryManager {
    private static final String BASE_URL = "http://api.worldweatheronline.com";
    private static WeatherAPI weatherAPI;

    static {
        initialize();
    }

    private static void initialize() {
        RestAdapter adapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(BASE_URL)
                .build();

        weatherAPI = adapter.create(WeatherAPI.class);
    }

    public static void getWeather(Callback<WeatherModel> callback) {
        weatherAPI.getWeather(callback);
    }

    private interface WeatherAPI {
        @GET("/premium/v1/weather.ashx?key=749be35c60fa4b8e8bf135809161511&q=Kharkiv&format=json&num_of_days=5&mca=no&tp=24")
        void getWeather(Callback<WeatherModel> callback);
    }
}
